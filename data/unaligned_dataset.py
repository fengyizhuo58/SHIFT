import os.path
from data.base_dataset import BaseDataset, get_transform
from data.image_folder import make_dataset
from PIL import Image
import random
import numpy as np
import sys
import csv
from imgaug import imgaug as ia
from imgaug import augmenters as iaa
from natsort import natsorted
from skimage.morphology import square
from skimage.filters import median
from skimage import img_as_ubyte

class UnalignedDataset(BaseDataset):
    """
    This dataset class can load aligned/paired datasets where A.shape != B.shape.

    It requires two directories to host training images from domain A '/path/to/data/trainA'
    and from domain B '/path/to/data/trainB' respectively.
    You can train the model with the dataset flag '--dataroot /path/to/data'.
    Similarly, you need to prepare two directories:
    '/path/to/data/testA' and '/path/to/data/testB' during test time.
    """

    def __init__(self, opt):
        """Initialize this dataset class.

        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseDataset.__init__(self, opt)
        self.dir_A = os.path.join(opt.dataroot, opt.phase + 'A')  # create a path '/path/to/data/trainA'
        self.dir_B = os.path.join(opt.dataroot, opt.phase + 'B')  # create a path '/path/to/data/trainB'

        self.A_paths = natsorted(make_dataset(self.dir_A, opt.max_dataset_size))   # load images from '/path/to/data/trainA'
        self.B_paths = natsorted(make_dataset(self.dir_B, opt.max_dataset_size))    # load images from '/path/to/data/trainB'
        self.A_size = len(self.A_paths)  # get the size of dataset A
        self.B_size = len(self.B_paths)  # get the size of dataset B
        assert self.A_size == self.B_size, "Every instance of A must have a corresponding instance of B!"
        
        #if self.opt.lambdadapt:
        self.stain_key = [('CK','FITC'), ('SM','Cy3'), ('KI','Cy5'), ('DAPI','DAPI')]
        
#         self.thresh_dict = {}
#         with open(self.opt.thresh_path, newline='') as csvfile:
#             reader = csv.reader(csvfile)
#             for row in reader:
#                 self.thresh_dict[row[0]]=eval(row[1])
                    

    def __getitem__(self, index):
        """Return a data point and its metadata information.

        Parameters:
            index (int)      -- a random integer for data indexing

        Returns a dictionary that contains A, B, A_paths and B_paths
            A (tensor)       -- an image in the input domain
            B (tensor)       -- its corresponding image in the target domain
            A_paths (str)    -- image paths
            B_paths (str)    -- image paths
        """
        A_paths = self.A_paths[index % self.A_size]  # make sure index is within range
        B_paths = self.B_paths[index % self.B_size]  # make sure index is within range
        A_img = np.array(Image.open(A_paths).convert('RGB'))
        B_img = np.array(Image.open(B_paths).convert('L')) # TODO: accommodate multi-chan B

        ############################################################################
        ###### Take out once rgb2gray is removed from pre-processing workflow ######
        ############################################################################
        # rgb2gray weights: 0.2989 * R + 0.5870 * G + 0.1140 * B 
        if 'FITC' in B_paths:
            B_img = (B_img/0.5870)
        
        if 'Cy3' in B_paths:
            B_img = (B_img/0.1140)
        
        if 'Cy5' in B_paths:
            B_img = (B_img/0.2989)
        
        # Denoise
        B_img = median(B_img.clip(0,255).astype('uint8'),square(5))
                
        #if self.opt.lambdadapt:
        fn = os.path.basename(B_paths)
        subset = fn.rsplit('_',6)[0]
        stain = next((i[0] for i in self.stain_key if i[1] in fn), False)
        thresh = 0.25 #self.thresh_dict[subset][stain]
        B_prev = (B_img > thresh).sum()/(self.opt.crop_size**2)
        
        if self.opt.aug:

            aug = iaa.Sequential([

                iaa.Sometimes(0.25, 
                              iaa.GaussianBlur(sigma=(0, 1)),name='blur'),

                iaa.Sometimes(0.5,
                              iaa.OneOf([iaa.Fliplr(1.0),
                                         iaa.Flipud(1.0)])),

                iaa.Sometimes(0.5,
                              iaa.OneOf([iaa.PiecewiseAffine(scale=(0.01, 0.05),mode='reflect'),
                                         iaa.PerspectiveTransform(scale=(0.01, 0.1))])), 

                iaa.Sometimes(0.5,
                              iaa.OneOf([iaa.AdditiveGaussianNoise(loc=0,scale=(0.0, 0.025*255)),
                                         iaa.AdditivePoissonNoise(lam=(0,8))]),name='noise'),

                iaa.Affine(rotate=(-20, 20), mode='reflect'),

                iaa.AddToHueAndSaturation(value=(-4, 4), per_channel=True,name='add')
            ], 
                random_order=True)

            # apply the same transform to both A and B
            aug_det = aug.to_deterministic()
            A_img = aug_det.augment_image(A_img).copy()
            B_img = aug_det.augment_image(B_img,hooks=ia.HooksImages(activator=self.activator)).copy()
                
        # TODO: move all transforms to unaligned_dataset, preserving base_dataset for other uses
        self.A_transform = get_transform(self.opt, grayscale=(self.opt.input_nc == 1))
        self.B_transform = get_transform(self.opt, grayscale=(self.opt.output_nc == 1))
        
        # apply image transformation
        A = self.A_transform(A_img)
        B = self.B_transform(B_img[:,:,np.newaxis])
        
        #if self.opt.lambdadapt:
        return {'A': A, 'B': B, 'A_paths': A_paths, 'B_paths': B_paths, 'B_prev': B_prev}        

    def activator(self, images, augmenter, parents, default):
        return False if augmenter.name in ['blur','noise','add'] else default

    def __len__(self):
        """Return the total number of images in the dataset.

        A_size should equal B_size.
        """
        return max(self.A_size, self.B_size)